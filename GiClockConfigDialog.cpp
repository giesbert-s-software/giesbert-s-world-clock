/******************************************************************************
 * Name       : GiClockConfigDialog.h
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-12
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QGridLayout>
#include <QtGui/QTableView>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QCheckBox>
#include <QtGui/QLabel>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiClockConfigDialog.h"
#include "GiClockLine.h"
#include "GiClockDelegate.h"

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
GiClockConfigDialog::GiClockConfigDialog(const GiClockConfig& rClockConfig, 
                                         QWidget *parent) : 
    QDialog(parent),
    m_ClockConfig(rClockConfig),
    m_pModel(0),
    m_pView(0),
    m_pIconClock(0)
{
    QVBoxLayout* pLayout = new QVBoxLayout(this);
    pLayout->setMargin(4);
    pLayout->setSpacing(4);

    m_pModel = new GiConfigModel(m_ClockConfig, this);

    m_pView = new QTableView(this);
    pLayout->addWidget(m_pView);
    m_pView->verticalHeader()->hide();
    m_pView->setModel(m_pModel);
    m_pView->resizeColumnsToContents();
    m_pView->resizeRowsToContents();
    m_pView->horizontalHeader()->setStretchLastSection(true);
    m_pView->setSelectionMode(QAbstractItemView::SingleSelection);
    m_pView->setSelectionBehavior(QAbstractItemView::SelectRows);

    GiClockDelegate* pDelegate = new GiClockDelegate(this);
    m_pView->setItemDelegate(pDelegate);

    QHBoxLayout* pCheckLayout = new QHBoxLayout();
    pLayout->addLayout(pCheckLayout);
    QCheckBox* pCheckBox = new QCheckBox(tr("Show Analog Clock"), this);
    connect(pCheckBox, SIGNAL(toggled(bool)), this, SLOT(showAnalogClock(bool)));
    pCheckBox->setChecked(m_ClockConfig.m_bDisplayAnalogClock);
    pCheckLayout->addWidget(pCheckBox);

    pCheckBox = new QCheckBox(tr("Show Digital Clock"), this);
    connect(pCheckBox, SIGNAL(toggled(bool)), this, SLOT(showDigitalClock(bool)));
    pCheckBox->setChecked(m_ClockConfig.m_bDisplayDigitalClock);
    pCheckLayout->addWidget(pCheckBox);

    pCheckBox = new QCheckBox(tr("Show Custom Name"), this);
    connect(pCheckBox, SIGNAL(toggled(bool)), this, SLOT(showCustomName(bool)));
    pCheckBox->setChecked(m_ClockConfig.m_bDisplayCustomName);
    pCheckLayout->addWidget(pCheckBox);

    pCheckBox = new QCheckBox(tr("Show Time Zone Name"), this);
    connect(pCheckBox, SIGNAL(toggled(bool)), this, SLOT(showTziName(bool)));
    pCheckBox->setChecked(m_ClockConfig.m_bDisplayTziName);
    pCheckLayout->addWidget(pCheckBox);

    QGridLayout* pAdditionalLayout = new QGridLayout();
    pLayout->addLayout(pAdditionalLayout);

    QSpinBox* pLinesCount = new QSpinBox(this);
    pLinesCount->setRange(1, 4);
    pLinesCount->setValue(m_ClockConfig.m_nClockLines);
    connect(pLinesCount, SIGNAL(valueChanged(int)), this, SLOT(clockLinesCountChanged(int)));
    int nRow = 0;
    pAdditionalLayout->addWidget(new QLabel(tr("Number of clock lines:"), this), nRow, 0);
    pAdditionalLayout->addWidget(pLinesCount, nRow, 1);
    pAdditionalLayout->addItem(new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Minimum), nRow, 2);

    QSpinBox* pClockSize = new QSpinBox(this);
    pClockSize->setRange(16, 200);
    pClockSize->setValue(m_ClockConfig.m_nClockSize);
    connect(pClockSize, SIGNAL(valueChanged(int)), this, SLOT(clockSizeChanged(int)));
    pAdditionalLayout->addWidget(new QLabel(tr("Clock size:"), this), ++nRow, 0);
    pAdditionalLayout->addWidget(pClockSize, nRow, 1);

    m_pIconClock = new QSpinBox(this);
    m_pIconClock->setRange(0, m_ClockConfig.m_items.count() - 1);
    m_pIconClock->setValue(m_ClockConfig.m_nIconClock);
    connect(m_pIconClock, SIGNAL(valueChanged(int)), this, SLOT(iconClockChanged(int)));
    pAdditionalLayout->addWidget(new QLabel(tr("Clock in System Tray:"), this), ++nRow, 0);
    pAdditionalLayout->addWidget(m_pIconClock, nRow, 1);

    QDialogButtonBox* pBtnBox = new QDialogButtonBox(this);
    pBtnBox->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    pLayout->addWidget(pBtnBox);
    setLayout(pLayout);

    QPushButton* pBtn = new QPushButton(tr("Add"), pBtnBox);
    connect(pBtn, SIGNAL(clicked(bool)), this, SLOT(addClock()));
    pBtnBox->addButton(pBtn, QDialogButtonBox::ActionRole);

    pBtn = new QPushButton(tr("Remove"), pBtnBox);
    connect(pBtn, SIGNAL(clicked(bool)), this, SLOT(removeClock()));
    pBtnBox->addButton(pBtn, QDialogButtonBox::ActionRole);

    connect(pBtnBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(pBtnBox, SIGNAL(rejected()), this, SLOT(reject()));
}

GiClockConfigDialog::~GiClockConfigDialog()
{

}

//~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const GiClockConfig& GiClockConfigDialog::getConfig() const
{
    return m_ClockConfig;
}
//~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void GiClockConfigDialog::addClock()
{
    m_pModel->insertRow(m_pModel->rowCount());
    m_pIconClock->setRange(0, m_ClockConfig.m_items.count());
}

void GiClockConfigDialog::removeClock()
{
    QModelIndex idx = m_pView->currentIndex();
    if(idx.isValid())
    {
        m_pModel->removeRow(idx.row(), idx.parent());
        m_pIconClock->setRange(0, m_ClockConfig.m_items.count() - 1);
    }
}

void GiClockConfigDialog::showAnalogClock(bool bNewState)
{
    m_ClockConfig.m_bDisplayAnalogClock = bNewState;
}

void GiClockConfigDialog::showDigitalClock(bool bNewState)
{
    m_ClockConfig.m_bDisplayDigitalClock = bNewState;
}

void GiClockConfigDialog::showCustomName(bool bNewState)
{
    m_ClockConfig.m_bDisplayCustomName = bNewState;
}

void GiClockConfigDialog::showTziName(bool bNewState)
{
    m_ClockConfig.m_bDisplayTziName = bNewState;
}

void GiClockConfigDialog::clockLinesCountChanged(int nNewValue)
{
    m_ClockConfig.m_nClockLines = nNewValue;
    m_pModel->clockLinesChanged();
}

void GiClockConfigDialog::clockSizeChanged(int nNewValue)
{
    m_ClockConfig.m_nClockSize = nNewValue;
}

void GiClockConfigDialog::iconClockChanged(int nNewValue)
{
    m_ClockConfig.m_nIconClock = nNewValue;
}
//~~~~~ protected: overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ private: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
