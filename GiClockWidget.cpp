/******************************************************************************
 * Name       : GiClockWidget.h
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-12
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtGui/QVBoxLayout>
#include <QtGui/QLabel>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiClockWidget.h"
#include "GiClock.h"

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
GiClockWidget::GiClockWidget(QWidget *parent) :
    QWidget(parent),
    m_pClock(0),
    m_pLabel(0),
    m_pTziLabel(0)
{
    QVBoxLayout* pLayout = new QVBoxLayout(this);
    pLayout->setAlignment(Qt::AlignCenter);
    pLayout->setMargin(0);
    m_pClock = new GiClock(this);
    m_pLabel = new QLabel(this);
    m_pLabel->setText(tr("local time"));
    m_pTziLabel = new QLabel(this);
    m_pTziLabel->setVisible(false);
    m_pDigitLabel = new QLabel(this);
    m_pDigitLabel->setVisible(false);

    pLayout->addWidget(m_pLabel, 0, Qt::AlignCenter);
    pLayout->addWidget(m_pClock, 0, Qt::AlignCenter);
    pLayout->addWidget(m_pDigitLabel, 0, Qt::AlignCenter);
    pLayout->addWidget(m_pTziLabel, 0, Qt::AlignCenter);
    pLayout->setSizeConstraint(QLayout::SetFixedSize);
    setLayout(pLayout);
    setAutoFillBackground(false);
}

//~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ private: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

