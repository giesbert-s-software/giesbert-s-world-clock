#-------------------------------------------------
#
# Project created by QtCreator 2010-08-12T21:36:18
#
#-------------------------------------------------

QT       += core gui

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
TARGET = GiWorldClock
TEMPLATE = app
# for static build use:
CONFIG -= debug
CONFIG += release static
# instead of
# CONFIG += debug_and_release build_all
CONFIG += warn_on qt stl help

static { # everything below takes effect with CONFIG += static
    CONFIG += static staticlib
    DEFINES += STATIC
    message("~~~ static build ~~~")
    mac: TARGET = $$join(TARGET,,,_static)
    win32: TARGET = $$join(TARGET,,,s)
}

OTHER_FILES += GiWorldClock.rc \
               Resources/Style.qss \
               GiWorldClock.qhp \
               GiWorldClock.qhcp \
               help/index.html \
               help/doc.html \
               __makeQtHelp.bat \
               __makeNLS.bat \
    help/configuration.html \
    help/images/Restore.png \
    help/images/Pin.png \
    help/images/Minimize.png \
    help/images/main.png \
    help/images/Help.png \
    help/images/Exit.png \
    help/images/Configuration.png \
    help/images/config.png \
    help/images/About.png

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
HEADERS += \
    GiWorldClock.h \
    GiTimeZoneInfo.h \
    GiClockLine.h \
    GiClockConfigDialog.h \
    GiClock.h \
    GiConfigModel.h \
    GiClockDelegate.h \
    GiClockWidget.h \
    GiClockConfig.h \
    GiHelpBrowser.h \
    GiSplashScreen.h \
    version.h

SOURCES += \
    GiWorldClock.cpp \
    main.cpp \
    GiTimeZoneInfo.cpp \
    GiClockLine.cpp \
    GiClockConfigDialog.cpp \
    GiClock.cpp \
    GiConfigModel.cpp  \
    GiClockDelegate.cpp \
    GiClockWidget.cpp \
    GiClockConfig.cpp \
    GiSplashScreen.cpp \
    GiHelpBrowser.cpp

RESOURCES += \
    GiWorldClock.qrc

# files used to translate the application
TRANSLATIONS = GiWorldClock_de.ts \
               GiWorldClock_en.ts

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
VERSION = 01.00.00

# the windows specific resource file, used for version info and application icon
win32 {
    RC_FILE = GiWorldClock.rc
}

# change the nama of the binary, if it is build in debug mode
CONFIG(debug, debug|release) {
     mac: TARGET = $$join(TARGET,,,_debug)
     win32: TARGET = $$join(TARGET,,,d)
}

#define the directory, where the binary is placed
CONFIG(debug, debug|release) {
    DESTDIR = ../debug
}
else {
    DESTDIR = ../release
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Hook our build to add a pre pre build target into the build system between qmake's
# Makefile update and the actual project target.
# this idea comes from: http://colby.id.au/node/145
buildhook.depends =
CONFIG(debug,debug|release):buildhook.target = Makefile.Debug
CONFIG(release,debug|release):buildhook.target = Makefile.Release
QMAKE_EXTRA_TARGETS += buildhook


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create our custom build number target as pre pre build step.
# this idea comes from: http://colby.id.au/node/145
win32:versionbuild.commands = ..\tools\AutoIncreaseBuildNr.exe $$PWD\version.h
else:versionbuild.commands = ../tools/AutoIncreaseBuildNr $$PWD/version.h
buildhook.depends += versionbuild
QMAKE_EXTRA_TARGETS += versionbuild

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# build the online help as pre pre build step
onlinehelp1.name = building the onlien help part 1
onlinehelp1.commands = qhelpgenerator $$PWD\GiWorldClock.qhp -o $$DESTDIR\GiWorldClock.qch
onlinehelp1.target = $$DESTDIR\GiWorldClock.qch
onlinehelp1.input = GiWorldClock.qhp
onlinehelp1.output = GiWorldClock.qch
buildhook.depends += onlinehelp1
QMAKE_EXTRA_TARGETS += onlinehelp1
QMAKE_EXTRA_COMPILERS += onlinehelp1

onlinehelp2.commands = qcollectiongenerator $$PWD\GiWorldClock.qhcp -o $$DESTDIR\GiWorldClock.qhc
onlinehelp2.target = $$DESTDIR\GiWorldClock.qhc
onlinehelp2.input = GiWorldClock.qhcp
onlinehelp2.output = GiWorldClock.qhc
buildhook.depends += onlinehelp2
QMAKE_EXTRA_TARGETS += onlinehelp2
QMAKE_EXTRA_COMPILERS += onlinehelp2
