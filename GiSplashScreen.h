//*****************************************************************************
// Name       : GiTimeZoneInfo.h
// Description:
// Author     : Gerolf Reinwardt
// Date       : 2010-8-30
// Copyright c: 2010 by Giesbert's Softwareschmiede
// History :
// ----------------------------------------------------------------------------

#ifndef GISPLASHSCREEN_H
#define GISPLASHSCREEN_H

// ----- general includes --------------------------------------------------------------------------
#include <QtGui/QWidget>
// ----- local includes ----------------------------------------------------------------------------

// ----- pre defines -------------------------------------------------------------------------------
class QLabel;
class QProgressBar;

// ----- class definition --------------------------------------------------------------------------
/**
 *
 */
class GiSplashScreen : public QWidget
{
    Q_OBJECT

public:
    // ----- construction --------------------------------------------------------------------------
    explicit GiSplashScreen(const QPixmap& rpixImage = QPixmap(), Qt::WindowFlags eFlags = Qt::WindowStaysOnTopHint);
    virtual ~GiSplashScreen();

    // ----- operators -----------------------------------------------------------------------------
    // ----- methods -------------------------------------------------------------------------------
    void finish(QWidget* pWidget);
    virtual void repaint();
    void sleep(int nMSecs);

    // ----- accessors -----------------------------------------------------------------------------
    void setPixmap(const QPixmap& rpixImage);
    const QPixmap pixmap() const;
    void setStepCount(int nStepCount);
    int currentStep();

    // ----- members -------------------------------------------------------------------------------

public slots:
    // ----- public slots --------------------------------------------------------------------------
    void showStep(int nStep, const QString& rszMessage);
    void incrStep(const QString& rszMessage);
    void showMessage(const QString& rszMessage);
    void clearMessage();

signals:
    // ----- signals -------------------------------------------------------------------------------
    void messageChanged(const QString& rszMessage);

protected slots:
    // ----- protected slots -----------------------------------------------------------------------

    // ----- events --------------------------------------------------------------------------------
protected:
    bool event(QEvent* pEvent);
    void mousePressEvent(QMouseEvent* pMouseEvent);

private slots:
    // ----- private slots -------------------------------------------------------------------------

private:
    // ----- private helpers ------------------------------------------------------------------------
    void adaptPosition();
    // ----- members -------------------------------------------------------------------------------
    QLabel*         m_pImageLabel;
    QLabel*         m_pMessageLabel;
    QProgressBar*   m_pProgress;

    // ----- not allowed members -------------------------------------------------------------------
    Q_DISABLE_COPY(GiSplashScreen);
};

#endif // GISPLASHSCREEN_H


