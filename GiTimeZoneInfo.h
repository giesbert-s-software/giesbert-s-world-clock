//*****************************************************************************
// Name       : GiTimeZoneInfo.h
// Description:
// Author     : Gerolf Reinwardt
// Date       : 2008-9-10
// Copyright c: 2010 by Giesbert's Softwareschmiede
// History :
// ----------------------------------------------------------------------------
//*****************************************************************************

#ifndef _INCLUDE_GiTimeZoneInfo_H
#define _INCLUDE_GiTimeZoneInfo_H

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <windows.h>
#include <QtCore/QString>
#include <QtCore/QVector>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

// ~~~~~ pre defines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

/**
 *  specific time zone information like daylight switch, bias etc
 */
struct regTZI
{
    /**
     *  default offset to UTC
     */
    long m_nBias;

    /**
     *  default offset to UTC in addition to m_nBias when no daylight saving 
     *  time is active
     */
    long m_nStandardBias;

    /**
     *  offset to UTC in addition to m_nBias when daylight saving time is active
     */
    long m_nDaylightBias;

    /**
     *  switching date, when the standard time gets active
     */
    SYSTEMTIME StandardDate; 

    /**
     *  switching date, when the daylight saving time gets active
     */
    SYSTEMTIME DaylightDate;
};

/**
 *  this class holds information about a time zone.
 */
class GiTimeZoneInfo
{
public:
    //~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    /**
     *  constructs an empty time zone information object
     */
    GiTimeZoneInfo();

    //~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    /**
     *  reads the time zone information and fills the structure
     */
    void init(const QString& rszTimeZoneName);

    /**
     *  reads the time zone information and fills the structure
     */
    void initTzDisplayName(const QString& rszTimeZoneDisplayName);

    //~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    //~~~~~ member variables ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    QString               m_szName;           /**< name of the time zone */
    QString               m_szDisplay;        /**< display name of the time zone */
    QString               m_szDlt;            /**< name of the daylight part of the time zone */
    QString               m_szStd;            /**< standard name of the time zone */
    TIME_ZONE_INFORMATION m_TimeZoneInfo;     /**< windows specific time zone information */
};

/**
 *  this class holds information about a time zone.
 */
class GiTimeZoneInfoList
{
public:
    //~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    /**
     *  constructs an empty time zone information object
     */
    GiTimeZoneInfoList();

    //~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    /**
     *  reads the time zone information and fills the structure
     */
    void init(void);

    //~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    //~~~~~ member variables ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    QVector<GiTimeZoneInfo> m_timeZones;
};

#endif //_INCLUDE_GiTimeZoneInfo_H
