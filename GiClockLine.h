/******************************************************************************
 * Name       : GiClockLine.cpp
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-12
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

#pragma once

#ifndef __GiClockLine_h__
#define __GiClockLine_h__

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtGui/QWidget>
#include <QtCore/QVector>
#include <QtCore/QPointer>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiTimeZoneInfo.h"
#include "GiClockConfig.h"

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~ defines and types ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
class GiClockWidget;
class QHBoxLayout;
class QVBoxLayout;

/**
 *  
 */
class GiClockLine : public QWidget
{
    Q_OBJECT
public:
    //~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    GiClockLine(QWidget *parent);
    ~GiClockLine();

    //~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    const GiClockConfig& clockConfig();
    void setClockConfig(const GiClockConfig& rClockConfig);
    int getClockIndexFromPos(const QPoint& ptGlobal);
    GiClockWidget* getClockByIndex(int nIndex);
    void deleteClock(GiClockWidget* pClockWidget);
    void dropClock(const QString& rszDisplayName, const QString& rszTimeZone, const QPoint& rptOffset);
    //~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

signals:
    void timeChanged(const QString&, const QVector<QTime>&);

protected:
    //~~~~~ virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    virtual void timerEvent(QTimerEvent* pEvent);
    virtual void paintEvent(QPaintEvent* pEventData);

    //~~~~~ helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
private:
    //~~~~~ helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    void configureClock(QPointer<GiClockWidget> pClock, int nIndex);
    void updateClock();

    //~~~~~ member variables ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    QVector<QPointer<GiClockWidget> >   m_clockVector;
    QVector<QPointer<QHBoxLayout> >     m_lineLayouts;
    QVBoxLayout*                        m_pMainLayout;
    int                                 m_nTimerID;
    GiClockConfig                       m_config;
    //~~~~~ forbidden members ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
};

#endif // __GiClockLine_h__
