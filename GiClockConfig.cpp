/******************************************************************************
 * Name       : GiClockConfig.cpp
 * Description:
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-12
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History :
 *****************************************************************************/

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiClockConfig.h"

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
GiClockConfigItem::GiClockConfigItem() :
    m_szDisplayName(),
    m_nClockLine(0),
    m_timeZone()
{

}

GiClockConfig::GiClockConfig() :
    m_items(),
    m_bDisplayAnalogClock(true),
    m_bDisplayDigitalClock(true),
    m_bDisplayCustomName(true),
    m_bDisplayTziName(false),
    m_nClockLines(1),
    m_nClockSize(48),
    m_nIconClock(0)
{

}

QString GiClockConfig::localTimeZoneName(void)
{
    TIME_ZONE_INFORMATION tzi;
    int nTimeZoneId = ::GetTimeZoneInformation(&tzi);

    if(TIME_ZONE_ID_UNKNOWN != nTimeZoneId)
    {
        return QString::fromUtf16((const ushort*)tzi.StandardName);
    }
    return QString::null;
}

//~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ private: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

