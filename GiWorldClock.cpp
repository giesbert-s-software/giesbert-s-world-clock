/******************************************************************************
 * Name       : worldclock.h
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-12
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtGui/QApplication>
#include <QtGui/QToolBar>
#include <QtGui/QAction>
#include <QtGui/QVBoxLayout>
#include <QtCore/QSettings>
#include <QtGui/QCloseEvent>
#include <QtGui/QMenu>
#include <QtGui/QAction>
#include <QtGui/QPixmap>
#include <QtGui/QPainter>
#include <QtGui/QIcon>
#include <QtCore/QTime>
#include <QtGui/QDragEnterEvent>
#include <QtGui/QDragMoveEvent>
#include <QtGui/QDropEvent>
#include <QtGui/QMouseEvent>
#include <QtCore/QMimeData>
#include <QtGui/QDrag>
#include <QtGui/QLabel>
#include <QtGui/QMessageBox>
#include <QtHelp/QHelpEngine>
#include <QtHelp/QHelpContentModel>
#include <QtHelp/QHelpContentWidget>
#include <QtHelp/QHelpIndexModel>
#include <QtHelp/QHelpIndexWidget>
#include <QtHelp/QHelpSearchEngine>
#include <QtGui/QTabWidget>
#include <windows.h>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiWorldClock.h"
#include "GiClockLine.h"
#include "GiClockConfigDialog.h"
#include "GiClockWidget.h"
#include "GiClock.h"
#include "GiHelpBrowser.h"
#include <version.h>

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
GiWorldClock::GiWorldClock(QWidget *parent, Qt::WFlags flags) :
    QWidget(parent, flags),
    m_pClockLine(0),
    m_bLockPosition(false),
    m_pTrayIcon(0),
    m_pTrayMenu(0),
    m_pRestoreAction(0),
    m_pQuitAction(0),
    m_pMinAction(0),
    m_pAboutAction(0),
    m_pHelpAction(0),
    m_pHelpEngine(0),
    m_pHelpWindow(0)
{
    QVBoxLayout* pLayout = new QVBoxLayout(this);

    pLayout->setMargin(0);
    pLayout->setSpacing(0);

    QToolBar* pToolBar = new QToolBar(this);
    pToolBar->setIconSize(QSize(16,16));
    pLayout->addWidget(pToolBar);

    createActions();
    createTrayIcon();

    QString szPath = QApplication::applicationDirPath();
    szPath += "/GiWorldClock.qhc";
    m_pHelpEngine = new QHelpEngine(szPath, this);
    bool bHelpAvailable = m_pHelpEngine->setupData();

    if(bHelpAvailable)
    {
        m_pHelpWindow = new QSplitter(Qt::Horizontal);
        m_pHelpWindow->setWindowTitle(tr("Giesbert's World Clock - Online Help"));

        GiHelpBrowser* pHelpBrowser = new GiHelpBrowser(m_pHelpEngine);
        QTabWidget* pHelpTab = new QTabWidget();

        m_pHelpWindow->insertWidget(0, pHelpTab);
        pHelpTab->addTab(m_pHelpEngine->contentWidget(), tr("Content"));
        pHelpTab->addTab(m_pHelpEngine->indexWidget(), tr("Index"));
        m_pHelpWindow->insertWidget(1, pHelpBrowser);
        m_pHelpWindow->setStretchFactor(1, 1);

        connect(m_pHelpEngine->contentWidget(),
                SIGNAL(linkActivated(const QUrl&)),
                pHelpBrowser, SLOT(setSource(const QUrl&)));
        connect(m_pHelpEngine->indexWidget(),
                SIGNAL(linkActivated(const QUrl&, const QString&)),
                pHelpBrowser, SLOT(setSource(const QUrl&)));
        connect(m_pHelpAction, SIGNAL(triggered()), m_pHelpWindow, SLOT(show()));
        connect(m_pHelpAction, SIGNAL(triggered()), m_pHelpWindow, SLOT(raise()));
    }

    QAction* pPinAction = new QAction(QIcon(":/WorldClock/Pin.png"), tr("&Pin Position"), this);
    connect(pPinAction, SIGNAL(toggled(bool)), this, SLOT(changePinState(bool)));

    QAction* pConfigAction = new QAction(QIcon(":/WorldClock/Configuration.png"), tr("&Config"), this);
    connect(pConfigAction, SIGNAL(triggered(bool)), this, SLOT(doConfig()));

    pToolBar->addAction(pPinAction);
    pToolBar->addAction(pConfigAction);
    pToolBar->addAction(m_pMinAction);
    pToolBar->addAction(m_pQuitAction);
    pToolBar->addSeparator();
    pToolBar->addAction(m_pAboutAction);
    pToolBar->addAction(m_pHelpAction);

    pPinAction->setCheckable(true);
    pPinAction->setChecked(true);

    m_pClockLine = new GiClockLine(this);
    connect(m_pClockLine, SIGNAL(timeChanged(const QString&, const QVector<QTime>&)),
            this,         SLOT(timeChanged(const QString&, const QVector<QTime>&)));
    pLayout->addWidget(m_pClockLine);

    setLayout(pLayout);

    setWindowTitle(tr("World Clocks"));
    setWindowIcon(QIcon(":/WorldClock/CurrentTime.png"));

    Qt::WindowFlags eFlags = windowFlags();
    eFlags &= ~Qt::WindowMaximizeButtonHint;
    setWindowFlags(eFlags);

    resize(sizeHint());

    loadConfig();
    setAcceptDrops(true);
}

GiWorldClock::~GiWorldClock()
{
    delete m_pHelpWindow;
}

//~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
bool GiWorldClock::winEvent(MSG* p_pMessage, long* p_pnResult)
{
    switch(p_pMessage->message)
    {
    case WM_SYSCOMMAND:
        {
            if(m_bLockPosition && 
                ( (p_pMessage->wParam & 0xfff0) == SC_MOVE || 
                    (p_pMessage->wParam & 0xfff0) == SC_SIZE || 
                    (p_pMessage->wParam & 0xfff0) == SC_RESTORE))
            {
                *p_pnResult = 0;
                return true;
            }
        }
        break;
    }
    return false;
}

void GiWorldClock::closeEvent(QCloseEvent* pEvent)
{
    saveConfig();
    pEvent->accept();
    emit closeWindow();
}

void GiWorldClock::hideEvent(QHideEvent* pEvent)
{
    if(isMinimized())
    {
        ::ShowWindow(this->winId(), SW_HIDE);
        m_pRestoreAction->setEnabled(true);
        m_pMinAction->setDisabled(true);
    }
    QWidget::hideEvent(pEvent);
}

void GiWorldClock::showEvent(QShowEvent* pEvent)
{
    QWidget::showEvent(pEvent);
    ::ShowWindow(this->winId(), SW_SHOW);
    m_pRestoreAction->setDisabled(true);
    m_pMinAction->setEnabled(true);
}

void GiWorldClock::resizeEvent(QResizeEvent* pEvent)
{
    QWidget::resizeEvent(pEvent);
}

void GiWorldClock::dragEnterEvent(QDragEnterEvent* pEvent)
{
    if (pEvent->mimeData()->hasFormat("application/giworldclock"))
    {
        if (children().contains(pEvent->source())) 
        {
            pEvent->setDropAction(Qt::MoveAction);
            pEvent->accept();
        } 
        else 
        {
            pEvent->acceptProposedAction();
        }
    } 
    // TODO: dragEnterEvent-> check whether text might also be accepted... timezone name?
//     else if (pEvent->mimeData()->hasText()) 
//     {
//         pEvent->acceptProposedAction();
//     } 
    else 
    {
        pEvent->ignore();
    }
}

void GiWorldClock::dragMoveEvent(QDragMoveEvent* pEvent)
{
    if (pEvent->mimeData()->hasFormat("application/giworldclock"))
    {
        if (children().contains(pEvent->source())) 
        {
            pEvent->setDropAction(Qt::MoveAction);
            pEvent->accept();
        } 
        else 
        {
            pEvent->acceptProposedAction();
        }
    } 
    // TODO: dragMoveEvent-> check whether text might also be accepted... timezone name?
//     else if (pEvent->mimeData()->hasText()) 
//     {
//         pEvent->acceptProposedAction();
//     } 
    else 
    {
        pEvent->ignore();
    }
}

void GiWorldClock::dropEvent(QDropEvent* pEvent)
{
    if (pEvent->mimeData()->hasFormat("application/giworldclock"))
    {
        const QMimeData *mime = pEvent->mimeData();
        QByteArray itemData = mime->data("application/giworldclock");
        QDataStream dataStream(&itemData, QIODevice::ReadOnly);

        QString szDisplayName;
        QString szTimeZone;
        QPoint  ptOffset; 

        dataStream >> szDisplayName >> szTimeZone >> ptOffset;

        QPoint pt = pEvent->pos();
        pt -= m_pClockLine->geometry().topLeft();
        m_pClockLine->dropClock(szDisplayName, szTimeZone, pt);

        if (pEvent->source() == this) 
        {
            pEvent->setDropAction(Qt::MoveAction);
            pEvent->accept();
        } 
        else
        {
            pEvent->acceptProposedAction();
        }
    } 
    // TODO: dropEvent->  check whether text might also be accepted... timezone name?
//     else if (pEvent->mimeData()->hasText()) 
//     {
//         QStringList pieces = event->mimeData()->text().split(QRegExp("\\s+"),
//             QString::SkipEmptyParts);
//         QPoint position = event->pos();
// 
//         foreach (QString piece, pieces) 
//         {
//             DragLabel *newLabel = new DragLabel(piece, this);
//             newLabel->move(position);
//             newLabel->show();
//             newLabel->setAttribute(Qt::WA_DeleteOnClose);
// 
//             position += QPoint(newLabel->width(), 0);
//         }
// 
//         pEvent->acceptProposedAction();
//     } 
    else 
    {
        pEvent->ignore();
    }
}

void GiWorldClock::mousePressEvent(QMouseEvent* pEvent)
{
    int nIndex = m_pClockLine->getClockIndexFromPos(pEvent->globalPos());
    GiClockWidget* pClockWidget = m_pClockLine->getClockByIndex(nIndex);
    if (!pClockWidget)
        return;

    QPoint hotSpot = pEvent->pos() - pClockWidget->pos();
    const GiClockConfig& rConfig = m_pClockLine->clockConfig();

    const GiClockConfigItem& rItem = rConfig.m_items.at(nIndex);

    QByteArray itemData;
    QDataStream dataStream(&itemData, QIODevice::WriteOnly);
    dataStream << rItem.m_szDisplayName << rItem.m_timeZone.m_szName << QPoint(hotSpot);

    QPixmap pix(QPixmap::grabWidget(pClockWidget));

    QMimeData* pMimeData = new QMimeData;
    pMimeData->setData("application/giworldclock", itemData);
    QString szTime = QString("time: %1\nname: %2\ntimezone: %3")
                        .arg(pClockWidget->m_pDigitLabel->text())
                        .arg(rItem.m_szDisplayName)
                        .arg(rItem.m_timeZone.m_szName);
    pMimeData->setText(szTime);
    pMimeData->setImageData(pix);
    QString szTimeHTML = QString("<table><tr><td>time:</td><td>%1</td></tr>\n"\
                                        "<tr><td>name:</td><td>%2</td></tr>\n"\
                                        "<tr><td>timezone:</td><td>%3</td></tr></table>")
                                .arg(pClockWidget->m_pDigitLabel->text())
                                .arg(rItem.m_szDisplayName)
                                .arg(rItem.m_timeZone.m_szName);
    pMimeData->setHtml(szTimeHTML);

    QDrag* pDrag = new QDrag(this);
    pDrag->setMimeData(pMimeData);
    pDrag->setPixmap(pix);
    pDrag->setHotSpot(hotSpot);

    pClockWidget->hide();

    if (pDrag->exec(Qt::MoveAction | Qt::CopyAction, Qt::CopyAction) == Qt::MoveAction)
    {
        m_pClockLine->deleteClock(pClockWidget);
    }
    else
    {
        pClockWidget->show();
    }
}

//~~~~~ protected: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void GiWorldClock::loadConfig()
{
    QSettings settings;

    changePinState(false);
    GiTimeZoneInfoList  timeZoneInfoList;
    timeZoneInfoList.init();

    settings.beginGroup("General");
    resize(settings.value("size", size()).toSize());
    move(settings.value("pos", pos()).toPoint());
    bool bLockPosition = settings.value("pinned", m_bLockPosition).toBool();
    settings.endGroup();

    settings.beginGroup("Config");
    GiClockConfig cfg;
    cfg.m_bDisplayAnalogClock = settings.value("DisplayAnalogClock", cfg.m_bDisplayAnalogClock).toBool();
    cfg.m_bDisplayCustomName = settings.value("DisplayCustomName", cfg.m_bDisplayCustomName).toBool();
    cfg.m_bDisplayTziName = settings.value("DisplayTziName", cfg.m_bDisplayTziName).toBool();
    cfg.m_nClockLines = settings.value("ClockLinesCount", cfg.m_nClockLines).toInt();
    cfg.m_nClockSize = settings.value("ClockSize", cfg.m_nClockSize).toInt();
    cfg.m_nIconClock = settings.value("IconClock", cfg.m_nIconClock).toInt();
    int nCount = settings.value("ItemCount", 0).toInt();

    for(int i = 0; i < nCount; ++i)
    {
        GiClockConfigItem item;
        settings.beginGroup(QString("Item%1").arg(i, 4, 10, QLatin1Char('0')));
        item.m_szDisplayName = settings.value("DisplayName", item.m_szDisplayName).toString();
        item.m_timeZone.init(settings.value("TimeZoneName", item.m_timeZone.m_szName).toString());
        item.m_nClockLine = settings.value("ClockLine", item.m_nClockLine).toInt();

        settings.endGroup();

        cfg.m_items.append(item);
    }

    settings.endGroup();
    m_pClockLine->setClockConfig(cfg);
    changePinState(bLockPosition);
}

void GiWorldClock::saveConfig()
{
    QSettings settings;

    settings.beginGroup("General");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.setValue("pinned", m_bLockPosition);
    settings.endGroup();

    settings.beginGroup("Config");
    const GiClockConfig& rConfig = m_pClockLine->clockConfig();
    settings.setValue("DisplayAnalogClock", rConfig.m_bDisplayAnalogClock);
    settings.setValue("DisplayCustomName", rConfig.m_bDisplayCustomName);
    settings.setValue("DisplayTziName", rConfig.m_bDisplayTziName);
    settings.setValue("ClockLinesCount", rConfig.m_nClockLines);
    settings.setValue("ClockSize", rConfig.m_nClockSize);
    settings.setValue("IconClock", rConfig.m_nIconClock);
    settings.setValue("ItemCount", rConfig.m_items.count());
    
    for(int i = 0; i < rConfig.m_items.count(); ++i)
    {
        const GiClockConfigItem& rItem = rConfig.m_items.at(i);
        settings.beginGroup(QString("Item%1").arg(i, 4, 10, QLatin1Char('0')));
        settings.setValue("DisplayName", rItem.m_szDisplayName);
        settings.setValue("TimeZoneName", rItem.m_timeZone.m_szName);
        settings.setValue("ClockLine", rItem.m_nClockLine);
        settings.endGroup();
    }

    settings.endGroup();
}

void GiWorldClock::exitApp()
{
    close();
}

void GiWorldClock::doConfig()
{
    GiClockConfigDialog dlg(m_pClockLine->clockConfig(), this);
    if(QDialog::Accepted == dlg.exec())
    {
        m_pClockLine->setClockConfig(dlg.getConfig());
        resize(sizeHint());
    }
}

void GiWorldClock::changePinState(bool bNewState)
{
    Qt::WindowFlags eFlags = windowFlags();
    if(bNewState)
    {
        eFlags &= ~Qt::WindowCloseButtonHint;
        eFlags &= ~Qt::WindowMinimizeButtonHint;
        eFlags &= ~Qt::WindowTitleHint;
        eFlags |= Qt::FramelessWindowHint;
        eFlags |= Qt::CustomizeWindowHint;
        eFlags |= Qt::WindowStaysOnTopHint;

        m_bLockPosition = true;
    }
    else
    {
        eFlags |= Qt::WindowCloseButtonHint;
        eFlags |= Qt::WindowMinimizeButtonHint;
        eFlags |= Qt::WindowTitleHint;
        eFlags &= ~Qt::FramelessWindowHint;
        eFlags &= ~Qt::CustomizeWindowHint;
        eFlags &= ~Qt::WindowStaysOnTopHint;
        m_bLockPosition = false;
    }
    setWindowFlags(eFlags);
    show();
}

void GiWorldClock::trayIconActivated(QSystemTrayIcon::ActivationReason eReason)
{
    switch (eReason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
        if(isMinimized())
        {
            // Show from system tray
            ::ShowWindow(this->winId(), SW_SHOW);
            showNormal();
        }
        else
        {
            // Hide to system tray
            showMinimized();
            ::ShowWindow(this->winId(), SW_HIDE);
        }
        break;
    case QSystemTrayIcon::MiddleClick:
    default:
        break;
    }

}

void GiWorldClock::timeChanged(const QString&        rszTimeAsText,
                               const QVector<QTime>& rTimeVec)
{
    m_pTrayIcon->setToolTip(rszTimeAsText);
    const GiClockConfig& rConfig = m_pClockLine->clockConfig();
    GiClockWidget* pWidget = m_pClockLine->getClockByIndex(rConfig.m_nIconClock);
    if(0 != pWidget)
    {
        QPixmap pix(QPixmap::grabWidget(pWidget->m_pClock));
        QIcon ico(pix);
        m_pTrayIcon->setIcon(ico);
    }
    else
    {
        m_pTrayIcon->setIcon(QIcon(":/WorldClock/CurrentTime.png"));
    }
}

void GiWorldClock::showAbout()
{
    QString szVersion = QString("%1.%2.%3.%4").arg(VER_MAJOR, 2, 10, QChar('0'))
                                              .arg(VER_MINOR, 2, 10, QChar('0'))
                                              .arg(VER_SP, 2, 10, QChar('0'))
                                              .arg(VER_BUILDNR, 4, 10, QChar('0'));
    QMessageBox::about(this, tr("Giesbert's World Clock"),
               tr("The <b>World Clock</b> application shows several<br>"
                  "clocks in different time zones. All things are<br>"
                  "configurable.<br>"
                  "Build: %1").arg(szVersion));
}

//~~~~~ private: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void GiWorldClock::createTrayIcon()
{
    m_pTrayMenu = new QMenu(this);
    m_pTrayMenu->addAction(m_pRestoreAction);
    m_pTrayMenu->addAction(m_pMinAction);
    m_pTrayMenu->addSeparator();
    m_pTrayMenu->addAction(m_pQuitAction);

    m_pTrayIcon = new QSystemTrayIcon(this);
    m_pTrayIcon->setContextMenu(m_pTrayMenu);
    m_pTrayIcon->setIcon(QIcon(":/WorldClock/CurrentTime.png"));
    m_pTrayIcon->setVisible(true);

    connect(m_pTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this,        SLOT(  trayIconActivated(QSystemTrayIcon::ActivationReason)));
}

void GiWorldClock::createActions()
{
    m_pRestoreAction = new QAction(QIcon(":/WorldClock/Restore.png"), tr("&Restore"), this);
    connect(m_pRestoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));
    m_pRestoreAction->setDisabled(true);

    m_pQuitAction = new QAction(QIcon(":/WorldClock/Exit.png"), tr("&Quit"), this);
    connect(m_pQuitAction, SIGNAL(triggered()), this, SLOT(exitApp()));

    m_pMinAction = new QAction(QIcon(":/WorldClock/Minimize.png"), tr("&Minimize"), this);
    connect(m_pMinAction, SIGNAL(triggered()), this, SLOT(showMinimized()));

    m_pHelpAction = new QAction(QIcon(":/WorldClock/Help.png"), tr("&Help"), this);

    m_pAboutAction = new QAction(QIcon(":/WorldClock/About.png"), tr("&About"), this);
    connect(m_pAboutAction, SIGNAL(triggered()), this, SLOT(showAbout()));
}

