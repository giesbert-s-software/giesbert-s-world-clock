/******************************************************************************
 * Name       : GuiWorldClock.cpp
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-12
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

#pragma once

#ifndef __worldclock_h__
#define __worldclock_h__

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtGui/QWidget>
#include <QtGui/QSystemTrayIcon>
#include <QtGui/QSplitter>
#include <QtCore/QPointer>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~ defines and types ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
class GiClockLine;
class QHelpEngine;

/**
 *  
 */
class GiWorldClock : public QWidget
{
    Q_OBJECT
public:
    //~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    GiWorldClock(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~GiWorldClock();

    //~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
protected:
    //~~~~~ virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    /**
     *  This method is called by the parent widgets when their winEvent is called
     *  
     *  @param p_pMessage   message to process
     *  @param p_pnResult   result
     *  
     *  @retval true, to stop this event, otherwise false
     */
    virtual bool winEvent(MSG* p_pMessage, long* p_pnResult);
    virtual void closeEvent(QCloseEvent* pEvent);
    virtual void hideEvent(QHideEvent* pEvent);
    virtual void showEvent(QShowEvent* pEvent);
    virtual void resizeEvent(QResizeEvent* pEvent);
    virtual void dragEnterEvent(QDragEnterEvent* pEvent);
    virtual void dragMoveEvent(QDragMoveEvent* pEvent);
    virtual void dropEvent(QDropEvent* pEvent);
    virtual void mousePressEvent(QMouseEvent* pEvent);

    //~~~~~ helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    void loadConfig();
    void saveConfig();

signals:
    void closeWindow();

protected slots:
    void exitApp();
    void doConfig();
    void changePinState(bool bNewState);
    void trayIconActivated(QSystemTrayIcon::ActivationReason);
    void timeChanged(const QString&, const QVector<QTime>&);
    void showAbout();

private:
    //~~~~~ helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    void createTrayIcon();
    void createActions();

    //~~~~~ member variables ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    GiClockLine*        m_pClockLine;
    bool                m_bLockPosition;
    QSystemTrayIcon*    m_pTrayIcon;
    QMenu*              m_pTrayMenu;
    QAction*            m_pRestoreAction;
    QAction*            m_pQuitAction;
    QAction*            m_pMinAction;
    QAction*            m_pAboutAction;
    QAction*            m_pHelpAction;
    QHelpEngine*        m_pHelpEngine;
    QPointer<QSplitter> m_pHelpWindow;

    //~~~~~ forbidden members ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
};

#endif // __worldclock_h__
