//*****************************************************************************
// Name       : GiTimeZoneInfo.cpp
// Description:
// Author     : Gerolf Reinwardt
// Date       : 2008-9-10
// Copyright c: 2010 by Giesbert's Softwareschmiede
// History :
// ----------------------------------------------------------------------------
//*****************************************************************************

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtCore/QDebug>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiTimeZoneInfo.h"

// ~~~~~ pre defines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
GiTimeZoneInfo::GiTimeZoneInfo() :
    m_szName(),
    m_szDisplay(),
    m_szDlt(),
    m_szStd(),
    m_TimeZoneInfo()
{
    memset(&m_TimeZoneInfo, 0, sizeof(TIME_ZONE_INFORMATION));
}

GiTimeZoneInfoList::GiTimeZoneInfoList() :
    m_timeZones()
{
}

//~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void GiTimeZoneInfo::init(const QString& rszTimeZoneName)
{
    // Get time zone info from the registry
    QString szRegKey = QLatin1String("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Time Zones\\");
    szRegKey.append(rszTimeZoneName);
    m_szName = rszTimeZoneName;

    HKEY hTimeZoneKey = 0;
    long nError = RegOpenKeyExW(HKEY_LOCAL_MACHINE, (const wchar_t*)szRegKey.utf16(), 0, KEY_READ, &hTimeZoneKey);

    if(ERROR_SUCCESS == nError)
    {
        DWORD dwValueSize;
        char  pValue[512];
        regTZI regTimeZoneInfo;
        memset(&regTimeZoneInfo, 0, sizeof(regTZI));

        dwValueSize = 512;
        nError = RegQueryValueExW(hTimeZoneKey, L"Display", NULL, NULL, (LPBYTE)pValue, &dwValueSize);
        if(ERROR_SUCCESS == nError)
            m_szDisplay = QString::fromUtf16((const ushort*)pValue);
        dwValueSize = 512;
        nError = RegQueryValueExW(hTimeZoneKey, L"Dlt", NULL, NULL, (LPBYTE)pValue, &dwValueSize);
        if(ERROR_SUCCESS == nError)
            m_szDlt = QString::fromUtf16((const ushort*)pValue);
        dwValueSize = 512;
        nError = RegQueryValueExW(hTimeZoneKey, L"Std", NULL, NULL, (LPBYTE)pValue, &dwValueSize);
        if(ERROR_SUCCESS == nError)
            m_szStd = QString::fromUtf16((const ushort*)pValue);
        dwValueSize = 512;
        nError = RegQueryValueExW(hTimeZoneKey, L"TZI", NULL, NULL, (LPBYTE)pValue, &dwValueSize);
        if(ERROR_SUCCESS == nError)
            memcpy((BYTE*)&regTimeZoneInfo, pValue, dwValueSize);

        m_TimeZoneInfo.Bias = regTimeZoneInfo.m_nBias; // LONG
        if(32 > m_szStd.length())
            wcscpy(m_TimeZoneInfo.StandardName, (const wchar_t*)m_szStd.utf16());
        m_TimeZoneInfo.StandardDate = regTimeZoneInfo.StandardDate; // SYSTEMTIME
        m_TimeZoneInfo.StandardBias = regTimeZoneInfo.m_nStandardBias; // LONG
        if(32 > m_szStd.length())
            wcscpy(m_TimeZoneInfo.DaylightName, (const wchar_t*)m_szDlt.utf16());
        m_TimeZoneInfo.DaylightDate = regTimeZoneInfo.DaylightDate; // SYSTEMTIME
        m_TimeZoneInfo.DaylightBias = regTimeZoneInfo.m_nDaylightBias; // LONG
    }
}

void GiTimeZoneInfo::initTzDisplayName(const QString& rszTimeZoneDisplayName)
{
    GiTimeZoneInfoList  timeZoneInfoList;
    timeZoneInfoList.init();
    for(int i = 0; i < timeZoneInfoList.m_timeZones.count(); ++i)
    {
        if(timeZoneInfoList.m_timeZones.at(i).m_szDisplay == rszTimeZoneDisplayName)
        {
            *this = timeZoneInfoList.m_timeZones.at(i);
            break;
        }
    }
}

void GiTimeZoneInfoList::init(void)
{
    HKEY hTimeZoneBaseKey = 0;
    QString szRegKey = QLatin1String("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Time Zones\\");
    long nError = RegOpenKeyExW(HKEY_LOCAL_MACHINE, (const wchar_t*)szRegKey.utf16(), 0, KEY_READ, &hTimeZoneBaseKey);

    DWORD dwIndex = 0; 
    while(ERROR_SUCCESS == nError)
    {
        wchar_t pszNameBuffer[256];
        DWORD   wBufferSize = 256;
        memset(pszNameBuffer, 0, sizeof(pszNameBuffer));

        nError = RegEnumKeyExW(hTimeZoneBaseKey, dwIndex++, pszNameBuffer, &wBufferSize, 0, 0, 0, 0);

        if(ERROR_SUCCESS == nError)
        {
            GiTimeZoneInfo tzi;
            tzi.init(QString::fromUtf16((const ushort*)pszNameBuffer));
            m_timeZones.append(tzi);
        }
        else if(ERROR_NO_MORE_ITEMS != nError)
        {
            qDebug() << "error retrieving the time zone information";
        }
    }
}

//~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ public slots ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: events handlers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ private: slots ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ private: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

