/******************************************************************************
 * Name       : GiConfigModel.h
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-13
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiConfigModel.h"

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
GiConfigModel::GiConfigModel(GiClockConfig& rClockConfig,
                             QObject* pParent) :
    QAbstractTableModel(pParent),
    m_clockConfig(rClockConfig)
{

}

GiConfigModel::~GiConfigModel()
{

}

//~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
int GiConfigModel::rowCount(const QModelIndex& ridxParent) const
{
    if(ridxParent.isValid())
        return 0;
    return m_clockConfig.m_items.count();
}

int GiConfigModel::columnCount(const QModelIndex& ridxParent) const
{
    return 3;
}

QVariant GiConfigModel::data(const QModelIndex& rIndex, int eRole) const
{
    QVariant varItem;

    if(rIndex.isValid() &&
        ((Qt::DisplayRole == eRole) ||
         (Qt::EditRole == eRole)))
    {
        if(rIndex.column() == 0)
        {
            varItem = m_clockConfig.m_items.at(rIndex.row()).m_szDisplayName;
        }
        else if(rIndex.column() == 1)
        {
            varItem = m_clockConfig.m_items.at(rIndex.row()).m_timeZone.m_szDisplay;
        }
        else if(rIndex.column() == 2)
        {
            varItem = m_clockConfig.m_items.at(rIndex.row()).m_nClockLine;
        }
    }
    return varItem;
}

bool GiConfigModel::setData(const QModelIndex& rIndex, const QVariant& rValue, int eRole)
{
    if(rIndex.isValid() && (Qt::EditRole == eRole))
    {
        if(rIndex.column() == 0)
        {
            m_clockConfig.m_items[rIndex.row()].m_szDisplayName = rValue.toString();
            emit dataChanged(rIndex, rIndex);
            return true;
        }
        else if(rIndex.column() == 1)
        {
            m_clockConfig.m_items[rIndex.row()].m_timeZone.initTzDisplayName(rValue.toString());
            emit dataChanged(rIndex, rIndex);
            return true;
        }
        else if(rIndex.column() == 2)
        {
            int nValue = rValue.toInt();
            if((nValue < m_clockConfig.m_nClockLines) && (0 <= nValue))
            {
                m_clockConfig.m_items[rIndex.row()].m_nClockLine = nValue;
                emit dataChanged(rIndex, rIndex);
                return true;
            }
        }
    }
    return false;
}

QVariant GiConfigModel::headerData(int nSection, Qt::Orientation eOrientation, int eRole) const
{
    QVariant varItem;

    if(Qt::DisplayRole == eRole && Qt::Horizontal == eOrientation)
    {
        if(0 == nSection)
        {
            varItem = tr("Display Name");
        }
        else if(1 == nSection)
        {
            varItem = tr("Time Zone Name");
        }
        else if(2 == nSection)
        {
            varItem = tr("Clockline");
        }
    }
    return varItem;
}

Qt::ItemFlags GiConfigModel::flags(const QModelIndex& rIndex) const
{
    Qt::ItemFlags eFlags = Qt::NoItemFlags;
    if(rIndex.isValid())
    {
        eFlags = Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable;
    }
    return eFlags;
}

bool GiConfigModel::insertRows(int nRow, int nCount, const QModelIndex& rParent)
{
    if((0 <= nRow) && (nRow <= rowCount()) && (0 < nCount))
    {
        beginInsertRows(QModelIndex(), nRow, nRow + nCount - 1);

        GiClockConfigItem item;
        item.m_szDisplayName = tr("local time");
        item.m_timeZone.init(GiClockConfig::localTimeZoneName());
        m_clockConfig.m_items.insert(nRow, nCount, item);
        endInsertRows();

        return true;
    }
    return false;
}

bool GiConfigModel::removeRows(int nRow, int nCount, const QModelIndex& rParent)
{
    if((0 <= nRow) && ((nRow + nCount - 1) <= rowCount()) && (0 < nCount))
    {
        beginRemoveRows(QModelIndex(), nRow, nRow + nCount - 1);
        m_clockConfig.m_items.remove(nRow, nCount);
        endRemoveRows();
    }
    return false;
}

void GiConfigModel::clockLinesChanged()
{
    for(int nRow = 0; nRow < rowCount(); ++nRow)
    {
        if(m_clockConfig.m_nClockLines <= m_clockConfig.m_items.at(nRow).m_nClockLine)
        {
            m_clockConfig.m_items[nRow].m_nClockLine = m_clockConfig.m_nClockLines - 1;
            QModelIndex idx = createIndex(nRow, 2);
            emit dataChanged(idx, idx);
        }
    }
}

//~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const GiClockConfig& GiConfigModel::getConfig() const
{
    return m_clockConfig;
}

GiClockConfig& GiConfigModel::getConfig()
{
    return m_clockConfig;
}

//~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ private: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

