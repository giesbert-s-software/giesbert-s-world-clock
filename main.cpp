/******************************************************************************
 * Name       : main.h
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-12
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtGui/QApplication>
#include <QtCore/QFile>
#include <QtCore/QPointer>
#include <QtCore/QTranslator>
#include <QtCore/QLocale>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiWorldClock.h"
#include "GiSplashScreen.h"

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
int main(int argc, char *argv[])
{
    QApplication appObj(argc, argv);
    appObj.setOrganizationName("Giesbert's Software");
    appObj.setOrganizationDomain("Giesbert-s-Software.com");
    appObj.setApplicationName("World Clock");

    QFile styleFile(QLatin1String(":/WorldClock/Resources/Style.qss"));
    if(styleFile.open(QFile::ReadOnly))
    {
        QString szStyle;
        szStyle = styleFile.readAll();
        styleFile.close();
        appObj.setStyleSheet(szStyle);
    }

    QString szTranslatorFile;
    // load translation files
    QTranslator translator;
    QString locale = QLocale::system().name();
    szTranslatorFile = QLatin1String("GiWorldClock_") + locale + QLatin1String(".qm");

    if(!szTranslatorFile.isEmpty())
    {
        if(translator.load(szTranslatorFile))
            appObj.installTranslator(&translator);
    }

    // create splash screen
    QPixmap pixmap(":/WorldClock/Splash.png");
    GiSplashScreen splash(pixmap);
    splash.show();
    splash.setStepCount(3);
    splash.incrStep(GiWorldClock::tr("loading configuration"));
    splash.sleep(300);
    splash.incrStep(GiWorldClock::tr("processing configuration"));
    splash.sleep(300);
    splash.incrStep(GiWorldClock::tr("loading UI"));
    QPointer<GiWorldClock> pWorldClock = new GiWorldClock(0);
    splash.finish(pWorldClock);
    splash.sleep(300);

    pWorldClock->show();
    QObject::connect(pWorldClock, SIGNAL(closeWindow()), &appObj, SLOT(quit ()));
    int nRetVal = appObj.exec();
    delete pWorldClock;
    pWorldClock = 0;

    return nRetVal;
}

//~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ private: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

