/******************************************************************************
 * Name       : version.h
 * Description:
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-12
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History :
 *****************************************************************************/

#pragma once

#ifndef __version_h__
#define __version_h__

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#define VER_BUILDNR 257
#define VER_MAJOR 1
#define VER_MINOR 0
#define VER_SP 0

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~ defines and types ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //


#endif // __version_h__

