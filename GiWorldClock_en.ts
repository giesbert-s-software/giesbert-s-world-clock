<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>GiClockConfigDialog</name>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="60"/>
        <source>Show Analog Clock</source>
        <translation>Show Analog Clock</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="65"/>
        <source>Show Digital Clock</source>
        <translatorcomment>Digitale Uhr anzeigen</translatorcomment>
        <translation>Show Digital Cloc</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="70"/>
        <source>Show Custom Name</source>
        <translation>Show Custom Name</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="75"/>
        <source>Show Time Zone Name</source>
        <translation>Show Time Zone Name</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="88"/>
        <source>Number of clock lines:</source>
        <translation>Number of clock lines:</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="96"/>
        <source>Clock size:</source>
        <translation>Analog clock size:</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="103"/>
        <source>Clock in System Tray:</source>
        <translation>Show clock in System Tray:</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="111"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="115"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
</context>
<context>
    <name>GiClockLine</name>
    <message>
        <location filename="GiClockLine.cpp" line="46"/>
        <source>local time</source>
        <translation>local time</translation>
    </message>
</context>
<context>
    <name>GiClockWidget</name>
    <message>
        <location filename="GiClockWidget.cpp" line="34"/>
        <source>local time</source>
        <translation>local time</translation>
    </message>
</context>
<context>
    <name>GiConfigModel</name>
    <message>
        <location filename="GiConfigModel.cpp" line="108"/>
        <source>Display Name</source>
        <translation>Display Name</translation>
    </message>
    <message>
        <location filename="GiConfigModel.cpp" line="112"/>
        <source>Time Zone Name</source>
        <translation>Time Zone Name</translation>
    </message>
    <message>
        <location filename="GiConfigModel.cpp" line="116"/>
        <source>Clockline</source>
        <translation>Clockline</translation>
    </message>
    <message>
        <location filename="GiConfigModel.cpp" line="139"/>
        <source>local time</source>
        <translation>local time</translation>
    </message>
</context>
<context>
    <name>GiWorldClock</name>
    <message>
        <location filename="GiWorldClock.cpp" line="88"/>
        <source>Giesbert&apos;s World Clock - Online Help</source>
        <translation>Giesbert&apos;s World Clock - Online Help</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="94"/>
        <source>Content</source>
        <translation>Content</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="95"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="109"/>
        <source>&amp;Pin Position</source>
        <translation>&amp;Pin Position</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="112"/>
        <source>&amp;Config</source>
        <translation>&amp;Config</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="133"/>
        <source>World Clocks</source>
        <translation>World Clocks</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="530"/>
        <source>Giesbert&apos;s World Clock</source>
        <translation>Giesbert&apos;s World Clock</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="531"/>
        <source>The &lt;b&gt;World Clock&lt;/b&gt; application shows several&lt;br&gt;clocks in different time zones. All things are&lt;br&gt;configurable.&lt;br&gt;Build: %1</source>
        <translation>The &lt;b&gt;World Clock&lt;/b&gt; application shows several&lt;br&gt;clocks in different time zones. All things are&lt;br&gt;configurable.&lt;br&gt;Build: %1</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="557"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restore</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="561"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="564"/>
        <source>&amp;Minimize</source>
        <translation>&amp;Minimize</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="567"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="569"/>
        <source>&amp;About</source>
        <translation>&amp;About</translation>
    </message>
    <message>
        <location filename="main.cpp" line="61"/>
        <source>loading configuration</source>
        <translation>loading configuration</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>processing configuration</source>
        <translation>processing configuration</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>loading UI</source>
        <translation>loading U</translation>
    </message>
</context>
</TS>
