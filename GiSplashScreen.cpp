//*****************************************************************************
// Name       : GiSplashScreen.cpp
// Description:
// Author     : Gerolf Reinwardt
// Date       : 2008-9-10
// Copyright c: 2010 by Giesbert's Softwareschmiede
// History :
// ----------------------------------------------------------------------------
//*****************************************************************************

// ----- general includes --------------------------------------------------------------------------
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QVBoxLayout>
#include <QtCore/QThread>
#include <QtGui/QApplication>
#include <QtGui/QDesktopWidget>

// ----- local includes ----------------------------------------------------------------------------
#include "GiSplashScreen.h"

// ----- helper class ------------------------------------------------------------------------------
class GiMySleepThread : public QThread
{
public:
    static void mySleep(int nMSecs)
    {
        QThread::msleep(nMSecs);
    }
};

// ----- construction ------------------------------------------------------------------------------
GiSplashScreen::GiSplashScreen(const QPixmap& rpixImage, Qt::WindowFlags eFlags) :
        QWidget(0, Qt::SplashScreen | eFlags)
{
    QVBoxLayout* pLayout = new QVBoxLayout(this);
    pLayout->setMargin(1);
    m_pImageLabel = new QLabel(this);
    if(!rpixImage.isNull())
    {
        m_pImageLabel->setPixmap(rpixImage);
    }
    m_pMessageLabel = new QLabel(this);
    m_pProgress = new QProgressBar(this);
    m_pProgress->setRange(0,10);
    m_pProgress->setTextVisible(false);
    pLayout->addWidget(m_pImageLabel);
    pLayout->addWidget(m_pMessageLabel);
    pLayout->addWidget(m_pProgress);
    setLayout(pLayout);
    adaptPosition();
}

GiSplashScreen::~GiSplashScreen()
{
}

// ----- operators ---------------------------------------------------------------------------------
// ----- methods -----------------------------------------------------------------------------------
void GiSplashScreen::finish(QWidget* pWidget)
{
    if (pWidget)
    {
#if defined(Q_WS_X11)
        extern void qt_x11_wait_for_window_manager(QWidget *mainWin);
        qt_x11_wait_for_window_manager(pWidget);
#endif
    }
    adaptPosition();
    close();
}

void GiSplashScreen::repaint()
{
    QWidget::repaint();
    QApplication::flush();
}

void GiSplashScreen::sleep(int nMSecs)
{
    GiMySleepThread::mySleep(nMSecs);
}

// ----- accessors ---------------------------------------------------------------------------------
void GiSplashScreen::setPixmap(const QPixmap& rpixImage)
{
    m_pImageLabel->setPixmap(rpixImage);
    adaptPosition();
    repaint();
}

const QPixmap GiSplashScreen::pixmap() const
{
    const QPixmap* pPixmap = m_pImageLabel->pixmap();

    if(0 != pPixmap)
    {
        return *pPixmap;
    }
    return QPixmap();
}

void GiSplashScreen::setStepCount(int nStepCount)
{
    m_pProgress->setRange(0,nStepCount);
    adaptPosition();
    repaint();
}

int GiSplashScreen::currentStep()
{
    return m_pProgress->value();
}

// ----- members -----------------------------------------------------------------------------------
// ----- public slots ------------------------------------------------------------------------------
void GiSplashScreen::showStep(int nStep, const QString& rszMessage)
{
    m_pProgress->setValue(nStep);
    m_pMessageLabel->setText(rszMessage);
    adaptPosition();
    emit messageChanged(rszMessage);
    repaint();
}

void GiSplashScreen::incrStep(const QString& rszMessage)
{
    m_pProgress->setValue(m_pProgress->value() + 1);
    m_pMessageLabel->setText(rszMessage);
    adaptPosition();
    emit messageChanged(rszMessage);
    repaint();
}

void GiSplashScreen::showMessage(const QString& rszMessage)
{
    m_pMessageLabel->setText(rszMessage);
    emit messageChanged(rszMessage);
    repaint();
}

void GiSplashScreen::clearMessage()
{
    m_pMessageLabel->clear();
    emit messageChanged(QString());
}

// ----- protected slots ---------------------------------------------------------------------------
// ----- events ------------------------------------------------------------------------------------
bool GiSplashScreen::event(QEvent* pEvent)
{
     return QWidget::event(pEvent);
}

void GiSplashScreen::mousePressEvent(QMouseEvent* pMouseEvent)
{
    hide();
    QWidget::mousePressEvent(pMouseEvent);
}

// ----- private slots -----------------------------------------------------------------------------
// ----- private helpers ---------------------------------------------------------------------------
void GiSplashScreen::adaptPosition()
{
    QSize sz = minimumSize();
    QRect r(0, 0, sz.width(), sz.height());
    resize(sz);
    move(QApplication::desktop()->screenGeometry().center() - r.center());
    qApp->processEvents();
}
