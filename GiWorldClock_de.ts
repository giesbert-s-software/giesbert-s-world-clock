<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>GiClockConfigDialog</name>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="60"/>
        <source>Show Analog Clock</source>
        <translation>Analoge Uhr anzeigen</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="65"/>
        <source>Show Digital Clock</source>
        <translation></translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="70"/>
        <source>Show Custom Name</source>
        <translation>Eigenen Namen anzeigen</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="75"/>
        <source>Show Time Zone Name</source>
        <translation>Zeitzohnennamen anzeigen</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="88"/>
        <source>Number of clock lines:</source>
        <translation>Anzahlm der Uhrenzeilen:</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="96"/>
        <source>Clock size:</source>
        <translation>Größe der analogen Uhren:</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="103"/>
        <source>Clock in System Tray:</source>
        <translation>Uhrenanzeige im System Tray:</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="111"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="GiClockConfigDialog.cpp" line="115"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>GiClockLine</name>
    <message>
        <location filename="GiClockLine.cpp" line="46"/>
        <source>local time</source>
        <translation>Lokale Uhrzeit</translation>
    </message>
</context>
<context>
    <name>GiClockWidget</name>
    <message>
        <location filename="GiClockWidget.cpp" line="34"/>
        <source>local time</source>
        <translation>Lokale Uhrzeit</translation>
    </message>
</context>
<context>
    <name>GiConfigModel</name>
    <message>
        <location filename="GiConfigModel.cpp" line="108"/>
        <source>Display Name</source>
        <translation>Anzeigename</translation>
    </message>
    <message>
        <location filename="GiConfigModel.cpp" line="112"/>
        <source>Time Zone Name</source>
        <translation>Name der Zeitzohne</translation>
    </message>
    <message>
        <location filename="GiConfigModel.cpp" line="116"/>
        <source>Clockline</source>
        <translation>Zeile der Uhr</translation>
    </message>
    <message>
        <location filename="GiConfigModel.cpp" line="139"/>
        <source>local time</source>
        <translation>Lokale Uhrzeit</translation>
    </message>
</context>
<context>
    <name>GiWorldClock</name>
    <message>
        <location filename="GiWorldClock.cpp" line="88"/>
        <source>Giesbert&apos;s World Clock - Online Help</source>
        <translation>Giesbert&apos;s Weltzeituhr - Onlinehilfe</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="94"/>
        <source>Content</source>
        <translation>Inhalt</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="95"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="109"/>
        <source>&amp;Pin Position</source>
        <translation>&amp;Position fixieren</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="112"/>
        <source>&amp;Config</source>
        <translation>&amp;Konfiguration</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="133"/>
        <source>World Clocks</source>
        <translation>Weltzeituhren</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="530"/>
        <source>Giesbert&apos;s World Clock</source>
        <translation>Giesbert&apos;s Weltzeituhr</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="531"/>
        <source>The &lt;b&gt;World Clock&lt;/b&gt; application shows several&lt;br&gt;clocks in different time zones. All things are&lt;br&gt;configurable.&lt;br&gt;Build: %1</source>
        <translation>&lt;b&gt;Giesbert&apos;s Weltzeituhr&lt;/b&gt; zeigt mehrere &lt;br&gt;Uhren in verschiedenen Zeitzonen an. Vieles ist &lt;br&gt;konfigurierbar.&lt;br&gt;Build: %1</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="557"/>
        <source>&amp;Restore</source>
        <translation>&amp;Wiederherstellen</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="561"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="564"/>
        <source>&amp;Minimize</source>
        <translation>&amp;Minimieren</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="567"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="GiWorldClock.cpp" line="569"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="main.cpp" line="61"/>
        <source>loading configuration</source>
        <translation>lade Konfiguration</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>processing configuration</source>
        <translation>bearbeite Konfiguration</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>loading UI</source>
        <translation>lade Benutzerinterface</translation>
    </message>
</context>
</TS>
