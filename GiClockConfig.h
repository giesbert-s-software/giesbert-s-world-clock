/******************************************************************************
 * Name       : GiClockConfig.cpp
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-12
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

#pragma once

#ifndef __GiClockConfig_h__
#define __GiClockConfig_h__

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtCore/QVector>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiTimeZoneInfo.h"

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~ defines and types ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
class GiClockConfigItem
{
public:
    //~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    GiClockConfigItem();

    //~~~~~ member variables ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    QString m_szDisplayName;
    int     m_nClockLine;
    GiTimeZoneInfo m_timeZone;
};

class GiClockConfig
{
public:
    //~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    GiClockConfig();
    static QString localTimeZoneName(void);

    //~~~~~ member variables ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    QVector<GiClockConfigItem> m_items;
    bool m_bDisplayAnalogClock;
    bool m_bDisplayDigitalClock;
    bool m_bDisplayCustomName;
    bool m_bDisplayTziName;
    int  m_nClockLines;
    int  m_nClockSize;
    int  m_nIconClock;
};

#endif // __GiClockConfig_h__
