/******************************************************************************
 * Name       : Giclock.h
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-6-11
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtCore/QTime>
#include <QtGui/QPainter>
#include <QtGui/QLinearGradient>
#include <QtGui/QAccessible>
#define _USE_MATH_DEFINES
#include <math.h> 

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "Giclock.h"

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
GiClock::GiClock(QWidget *parent) : 
    QWidget(parent),
    m_currentTime()
{
    setAttribute(Qt::WA_OpaquePaintEvent, false);
    setAutoFillBackground(false);
}

GiClock::~GiClock()
{

}

//~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
QTime GiClock::currentTime(void) const
{
    return m_currentTime;
}

//~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void GiClock::setCurrentTime(const QTime& rCurrTime)
{
    if(m_currentTime != rCurrTime)
    {
        m_currentTime = rCurrTime;
        update();
        QAccessible::updateAccessibility(this, 0, QAccessible::ValueChanged);
    }
}

//~~~~~ protected: virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void GiClock::paintEvent(QPaintEvent*)
{
    static const QPoint hourHand[3] = {
        QPoint(7, 8),
        QPoint(-7, 8),
        QPoint(0, -50)
    };
    static const QPoint minuteHand[3] = {
        QPoint(7, 8),
        QPoint(-7, 8),
        QPoint(0, -75)
    };

    QColor hourColor(100, 20, 100);
    QColor minuteColor(40, 40, 40, 191);

    int side = qMin(width(), height());

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.translate(width() / 2, height() / 2);
    painter.scale(side / 200.0, side / 200.0);

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // draw the outer boder of the clock
    painter.setPen(Qt::NoPen);
    QRectF rc(-96, -96, 192, 192);
    QLinearGradient outerGradient(rc.topLeft(), rc.bottomRight());
    outerGradient.setColorAt(0, QColor(93, 135, 241));
    outerGradient.setColorAt(1, QColor(23, 49, 143));

    painter.setBrush(outerGradient);
    painter.drawEllipse(rc);

    // draw the white inner part of the clock face
    rc.adjust(5, 5, -5, -5);
    QLinearGradient innerGradient(rc.topLeft(), rc.bottomRight());
    innerGradient.setColorAt(0.0, QColor(255, 255, 255));
    innerGradient.setColorAt(0.3, QColor(255, 255, 255));
    innerGradient.setColorAt(1.0, QColor(180, 180, 180));

    painter.setBrush(innerGradient);
    painter.drawEllipse(rc);

    // draw a filled area for the display of the seconds
    QLinearGradient secondGradient(rc.topLeft(), rc.bottomRight());
    //secondGradient.setColorAt(0.0, QColor(204, 215, 243));
    //secondGradient.setColorAt(0.3, QColor(204, 215, 243));
    //secondGradient.setColorAt(1.0, QColor(124, 159, 243));
    secondGradient.setColorAt(0.0, QColor(222, 245, 155));
    secondGradient.setColorAt(0.3, QColor(211, 242, 122));
    secondGradient.setColorAt(1.0, QColor(172, 207,  73));

    painter.setBrush(secondGradient);
    int nStartAt0 = 16 * 90;
    int nCurrentSecondPosition = -m_currentTime.second() * 6 * 16;
    painter.drawPie(rc, nStartAt0, nCurrentSecondPosition);

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // draw the hour hand
    painter.setPen(Qt::NoPen);
    painter.setBrush(hourColor);

    painter.save();
    painter.rotate(30.0 * ((m_currentTime.hour() + m_currentTime.minute() / 60.0)));
    painter.drawConvexPolygon(hourHand, 3);
    painter.restore();

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // draw the hour markers
    painter.setPen(hourColor);

    for (int i = 0; i < 12; ++i)
    {
        painter.drawLine(72, 0, 91, 0);
        painter.rotate(30.0);
    }

    painter.setPen(Qt::NoPen);
    painter.setBrush(minuteColor);

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // draw the minute hand
    painter.save();
    painter.rotate(6.0 * (m_currentTime.minute() + m_currentTime.second() / 60.0));
    painter.drawConvexPolygon(minuteHand, 3);
    painter.restore();

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // draw the minute markers
    painter.setPen(QColor(0,0,0, 192));

    for (int j = 0; j < 60; ++j)
    {
        if ((j % 5) != 0)
            painter.drawLine(81, 0, 91, 0);
        painter.rotate(6.0);
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // draw the needle point
    rc = QRect(-10, -10, 20, 20);
    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor(0,0,0, 192));
    painter.drawEllipse(rc);
}

//~~~~~ protected: overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ private: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
