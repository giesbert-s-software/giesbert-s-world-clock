/******************************************************************************
 * Name       : GiClockLine.h
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-12
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtCore/QTime>
#include <QtCore/QTimerEvent>
#include <QtGui/QHBoxLayout>
#include <QtGui/QVBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QPainter>
#include <QtGui/QLinearGradient>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiClockLine.h"
#include "GiClock.h"
#include "GiClockWidget.h"

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
GiClockLine::GiClockLine(QWidget *parent) : 
    QWidget(parent),
    m_clockVector(),
    m_lineLayouts(0),
    m_pMainLayout(0),
    m_nTimerID(0),
    m_config()
{
    m_pMainLayout = new QVBoxLayout(this);
    m_pMainLayout->setMargin(0);
    setLayout(m_pMainLayout);

    QPointer<QHBoxLayout> pLineLayout(new QHBoxLayout);
    m_lineLayouts.append(pLineLayout);
    m_pMainLayout->addLayout(pLineLayout);

    GiClockConfigItem item;
    item.m_szDisplayName = tr("local time");
    item.m_timeZone.init(GiClockConfig::localTimeZoneName());
    GiClockConfig cfg;
    cfg.m_items.append(item);
    setClockConfig(cfg);

    m_nTimerID = startTimer(1000);
}

void GiClockLine::configureClock(QPointer<GiClockWidget> pClock, int nIndex)
{
    if(nIndex < m_config.m_items.count())
    {
        pClock->m_pClock->setFixedSize(m_config.m_nClockSize, m_config.m_nClockSize);
        pClock->m_pTziLabel->setText(m_config.m_items[nIndex].m_timeZone.m_szDisplay);
        pClock->m_pLabel->setText(m_config.m_items[nIndex].m_szDisplayName);
        pClock->m_pTziLabel->setVisible(m_config.m_bDisplayTziName);
        pClock->m_pLabel->setVisible(m_config.m_bDisplayCustomName);
        pClock->m_pClock->setVisible(m_config.m_bDisplayAnalogClock);
        pClock->m_pDigitLabel->setVisible(m_config.m_bDisplayDigitalClock);


        QPalette pal = pClock->m_pDigitLabel->palette();
        pal.setColor(QPalette::Text, Qt::white);
        pal.setColor(QPalette::Foreground, Qt::white);
        pClock->m_pDigitLabel->setPalette(pal);
        pClock->m_pTziLabel->setPalette(pal);
        pClock->resize(pClock->sizeHint());
    }
}

GiClockLine::~GiClockLine()
{
    killTimer(m_nTimerID);
    m_nTimerID = 0;
}

//~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const GiClockConfig& GiClockLine::clockConfig()
{
    return m_config;
}

void GiClockLine::setClockConfig(const GiClockConfig& rClockConfig)
{
    // delete clocks that are not needed anymore
    while(rClockConfig.m_items.count() < m_clockVector.count())
    {
        delete m_clockVector.last();
        m_clockVector.pop_back();
    }

    // delete lines that are not needed anymore
    while(rClockConfig.m_nClockLines < m_lineLayouts.count())
    {
        delete m_lineLayouts.last();
        m_lineLayouts.pop_back();
    }

    // add additional lines if needed
    while(rClockConfig.m_nClockLines > m_lineLayouts.count())
    {
        QPointer<QHBoxLayout> pLineLayout(new QHBoxLayout);
        m_lineLayouts.append(pLineLayout);
        m_pMainLayout->addLayout(pLineLayout);
    }

    // add additional clocks if needed
    while(rClockConfig.m_items.count() > m_clockVector.count())
    {
        QPointer<GiClockWidget> pClock = new GiClockWidget(this);
        m_clockVector.append(pClock);
    }

    m_config = rClockConfig;

    for(int i = 0; i < m_config.m_items.count(); ++i)
    {
        const GiClockConfigItem& rItem = m_config.m_items.at(i);
        m_lineLayouts[rItem.m_nClockLine]->addWidget(m_clockVector[i]);
        configureClock(m_clockVector[i], i);
    }

    updateClock();
}

int GiClockLine::getClockIndexFromPos(const QPoint& ptGlobal)
{
    for(int i = 0; i < m_clockVector.count(); ++i)
    {
        QPoint ptLocal = m_clockVector.at(i)->mapFromGlobal(ptGlobal);
        if(m_clockVector.at(i)->rect().contains(ptLocal))
        {
            return i;
        }
    }

    return -1;
}

GiClockWidget* GiClockLine::getClockByIndex(int nIndex)
{
    if((0 <= nIndex) && (nIndex < m_clockVector.count()))
    {
        return m_clockVector[nIndex];
    }

    return 0;
}

void GiClockLine::deleteClock(GiClockWidget* pClockWidget)
{
    for(int i = 0; i < m_clockVector.count(); ++i)
    {
        if(m_clockVector[i] == pClockWidget)
        {
            m_clockVector.remove(i);
            m_config.m_items.remove(i);
            delete pClockWidget;
        }
    }
}

void GiClockLine::dropClock(const QString& rszDisplayName, const QString& rszTimeZone, const QPoint& rptOffset)
{
    GiClockConfigItem dropItem;
    GiClockConfig cfg(m_config);
    dropItem.m_szDisplayName = rszDisplayName;
    dropItem.m_timeZone.init(rszTimeZone);
    // 1.) evaluate, in which clock line the drop happens...
    int nHeightOfLine = m_clockVector.at(0)->height();
    dropItem.m_nClockLine = qMin(rptOffset.y() / nHeightOfLine, cfg.m_nClockLines - 1);

    int nInsertPosition = cfg.m_items.count();
    // 2.) evaluate, in which position the drop happens...
    for(int i = 0; i < cfg.m_items.count(); ++i)
    {
        // find the items in this clock line
        if(cfg.m_items.at(i).m_nClockLine == dropItem.m_nClockLine)
        {
            QRect rcClock = m_clockVector[i]->geometry();
            nInsertPosition = i;

            // found item that is the indicator to insert the clock item
            if(rcClock.contains(rptOffset))
            {
                // verify if the item has to be inserted before or after
                if(rptOffset.x() >= rcClock.center().x())
                {
                    ++nInsertPosition;
                }
                break;
            }
            else if(rptOffset.x() < rcClock.x())
            {
                // insert before
                break;
            }
        }
    }

    // 3.) reread the configuration
    cfg.m_items.insert(nInsertPosition, dropItem);
    m_clockVector.insert(nInsertPosition, new GiClockWidget(this));
    setClockConfig(cfg);
}

//~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void GiClockLine::timerEvent(QTimerEvent* pEvent)
{
    if(m_nTimerID == pEvent->timerId())
    {
        updateClock();
    }
}

void GiClockLine::paintEvent(QPaintEvent* /*pEventData*/)
{
    QPainter painter(this);
    QRectF rc(rect());
    QLinearGradient backGradient(rc.topLeft(), rc.bottomLeft());
    QColor col(128, 165, 252);


    backGradient.setColorAt(0,    QColor(176, 199, 254));
    backGradient.setColorAt(0.8,  QColor(128, 165, 252));
    backGradient.setColorAt(0.85, QColor(110, 152, 252));
    backGradient.setColorAt(0.9,  QColor(102, 132, 202));
    backGradient.setColorAt(0.95, QColor( 64,  82, 126));
    backGradient.setColorAt(1,    QColor( 53,  68, 105));

    painter.setBrush(backGradient);
    painter.fillRect(rc, backGradient);
}

void GiClockLine::updateClock()
{
    SYSTEMTIME sysTime;
    memset(&sysTime, 0, sizeof(SYSTEMTIME));
    GetSystemTime(&sysTime);
    QString         szCurrentTimeString;
    QVector<QTime>  timeVec;

    for(int i = 0; i < m_clockVector.count(); ++i)
    {
        QPointer<GiClockWidget>& rPointer = m_clockVector[i];
        SYSTEMTIME localTime;
        memset(&localTime, 0, sizeof(SYSTEMTIME));
        const GiClockConfigItem& rConfItem = m_config.m_items.at(i);
        TIME_ZONE_INFORMATION tzi = rConfItem.m_timeZone.m_TimeZoneInfo;
        SystemTimeToTzSpecificLocalTime(&tzi, &sysTime, &localTime);

        QTime currTime;
        currTime.setHMS(localTime.wHour, localTime.wMinute, localTime.wSecond, localTime.wMilliseconds);
        rPointer->m_pClock->setCurrentTime(currTime);
        QString szTime = currTime.toString(QLatin1String("HH:mm:ss"));
        rPointer->m_pDigitLabel->setText(szTime);

        if(!szCurrentTimeString.isEmpty())
            szCurrentTimeString.append(QLatin1String("\r\n"));
        szCurrentTimeString.append(rConfItem.m_szDisplayName);
        szCurrentTimeString.append(QLatin1String(" : "));
        szCurrentTimeString.append(szTime);
        timeVec.append(currTime);
    }

    emit timeChanged(szCurrentTimeString, timeVec);
}

//~~~~~ protected: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ private: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

