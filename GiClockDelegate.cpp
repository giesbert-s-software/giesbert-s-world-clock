/******************************************************************************
 * Name       : GiClockDelegate.cpp
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-16
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtGui/QComboBox>
#include <QtGui/QSpinBox>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiClockDelegate.h"
#include "GiConfigModel.h"

// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

//~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
GiClockDelegate::GiClockDelegate(QObject *parent) :
    QItemDelegate(parent),
    m_timeZoneInfoList()
{
    m_timeZoneInfoList.init();
}

GiClockDelegate::~GiClockDelegate()
{

}

//~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
QWidget* GiClockDelegate::createEditor(QWidget* pParent,
                                       const QStyleOptionViewItem& rOption,
                                       const QModelIndex& rIndex) const
{
    if((rIndex.isValid()) && (rIndex.column() == 1))
    {
        QComboBox* pCombo = new QComboBox(pParent);
        pCombo->setEditable(false);
        return pCombo;
    }
    else if((rIndex.isValid()) && (rIndex.column() == 2))
    {
        QSpinBox* pSpin = new QSpinBox(pParent);
        return pSpin;
    }
    return QItemDelegate::createEditor(pParent, rOption, rIndex);
}

void GiClockDelegate::setEditorData(QWidget* pEditor, const QModelIndex& rIndex) const
{
    if((rIndex.isValid()) && (rIndex.column() == 1))
    {
        QComboBox* pCombo = (QComboBox*)pEditor;
        QString szCurrentValue = rIndex.data().toString();
        QStringList szValues;
        for(int i = 0; i < m_timeZoneInfoList.m_timeZones.count(); ++i)
        {
            szValues.append(m_timeZoneInfoList.m_timeZones.at(i).m_szDisplay);
        }
        szValues.sort();
        pCombo->addItems(szValues);
        pCombo->setCurrentIndex(pCombo->findText(szCurrentValue));
    }
    if((rIndex.isValid()) && (rIndex.column() == 2))
    {
        QSpinBox* pSpin = (QSpinBox*)pEditor;
        GiConfigModel* pModel = (GiConfigModel*)rIndex.model();
        pSpin->setRange(0, pModel->getConfig().m_nClockLines - 1);
        pSpin->setValue(rIndex.data().toInt());
    }
    else
    {
        QItemDelegate::setEditorData(pEditor, rIndex);
    }
}

void GiClockDelegate::setModelData(QWidget* pEditor, QAbstractItemModel* pModel, const QModelIndex& rIndex) const
{
    if((rIndex.isValid()) && (rIndex.column() == 1))
    {
        QComboBox* pCombo = (QComboBox*)pEditor;
        pModel->setData(rIndex, pCombo->currentText());
    }
    else
    {
        QItemDelegate::setModelData(pEditor, pModel, rIndex);
    }
}

//~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ protected: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//~~~~~ private: helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
