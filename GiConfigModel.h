/******************************************************************************
 * Name       : GiConfigModel.cpp
 * Description: 
 * Author     : Gerolf Reinwardt
 * Date       : 2010-8-13
 * Copyright c: 2010 by Giesbert's Softwareschmiede
 * ----------------------------------------------------------------------------
 * History : 
 *****************************************************************************/

#pragma once

#ifndef __GiConfigModel_h__
#define __GiConfigModel_h__

// ~~~~~ global includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include <QtCore/QAbstractTableModel>

// ~~~~~ local includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~ predefines ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ~~~~~ defines and types ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
#include "GiClockConfig.h"

/**
 *  
 */
class GiConfigModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    //~~~~~ enums ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ constructors ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    GiConfigModel(GiClockConfig& rClockConfig, QObject* pParent);
    ~GiConfigModel();
    //~~~~~ operators ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    virtual int rowCount(const QModelIndex& ridxParent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex& ridxParent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex& rIndex, int eRole = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex& rIndex, const QVariant& rValue, int eRole = Qt::EditRole);
    virtual QVariant headerData(int nSection, Qt::Orientation eOrientation,
                                int eRole = Qt::DisplayRole) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;

    virtual bool insertRows(int nRow, int nCount, const QModelIndex& rParent = QModelIndex());
    virtual bool removeRows(int nRow, int nCount, const QModelIndex& rParent = QModelIndex());
    void clockLinesChanged();

    //~~~~~ access methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    const GiClockConfig& getConfig() const;
    GiClockConfig& getConfig();
    //~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
protected:
    //~~~~~ virtual methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ overwritten methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
private:
    //~~~~~ helpers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    //~~~~~ member variables ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    GiClockConfig& m_clockConfig;

    //~~~~~ forbidden members ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
};

#endif // __GiConfigModel_h__
